(function($){
  'use strict';
  var MQforPC = 'all and (min-width: 641px)';

  /*
  * メインビジュアル
  */
  $('.js-slider-mainvisual').each(function(){
    var $slider = $(this);
    if($slider.find('.slider').length >= 2){
      $slider.slick({
        useCSS: !cmnjs.UAINFO.IE,//win10 IE11で移動時overflow部分がちらつく
        slidesToShow:1,
        slidesToScroll:1,
        dots: true,
        arrows: false,
        autoplay:true,
        pauseOnFocus: false,
        autoplaySpeed:5000
      });
    }
  });

  /*
  * 投資信託 カルーセル
  */
  $('.js-slider-mainvisual-investment').each(function(){
    var $slider = $(this);
    if($slider.find('.slider').length >= 2){
      $slider.slick({
        useCSS: !cmnjs.UAINFO.IE,//win10 IE11で移動時overflow部分がちらつく
        slidesToShow:1,
        slidesToScroll:1,
        dots: true,
        arrows: false,
        autoplay:true,
        pauseOnFocus: false,
        autoplaySpeed:5000
      });
    }
  });

  /*
  * 重要なお知らせ
  */
  $('.js-slider-notices').each(function(){
    var $slider = $(this);
    if($slider.find('.slider').length >= 2){
      $slider.slick({
        useCSS: !cmnjs.UAINFO.IE,//win10 IE11で移動時overflow部分がちらつく
        slidesToShow:1,
        slidesToScroll:1,
        dots: false,
        arrows: true,
        autoplay:true,
        pauseOnFocus: false,
        autoplaySpeed:5000
      });
    }
  });

  /*
  * バナーカルーセル
  */
  $('.js-slider-banner').each(function(){
    var $slider = $(this);
    var PC_SHOW_SLIDE_LEN = $('.l-body-sub').length ? 2 : 3; // .l-body-subの有無でPC時の表示枚数
    var spSetting = {
      useCSS: !cmnjs.UAINFO.IE,//win10 IE11で移動時overflow部分がちらつく
      centerMode: true,
      centerPadding: '10%',
      slidesToShow:1,
      slidesToScroll:1,
      dots: true,
      arrows: false,
      autoplay:true,
      pauseOnFocus: false,
      autoplaySpeed:5000
    };
    var pcSetting = {
      useCSS: !cmnjs.UAINFO.IE,//win10 IE11で移動時overflow部分がちらつく
      slidesToShow:PC_SHOW_SLIDE_LEN,
      slidesToScroll:1,
      dots: true,
      arrows: true,
      autoplay:true,
      pauseOnFocus: false,
      autoplaySpeed:5000
    };
    function makeSlick(isPC){
      $slider.slick(isPC?pcSetting:spSetting);
    }

    if($slider.find('.slider').length > PC_SHOW_SLIDE_LEN){
      window.matchMedia(MQforPC).addListener(function (e) {
        if(e.matches){
          $slider.slick('unslick');
          makeSlick(true);
        }else{
          $slider.slick('unslick');
          makeSlick(false);
        }
      });
      makeSlick(window.matchMedia(MQforPC).matches);
    }
  });

})(jQuery);