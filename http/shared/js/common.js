(function($){
  'use strict';
  /*var
  ----------------------------------------------------------------------*/
  var DATAPREF = '-cmnjs';
  var globalKey = 'cmnjs';

  if(globalKey && window[globalKey]==null){
    window[globalKey]={};
  }else{
    globalKey = false;
  }

  var MQforPC = 'all and (min-width: 641px)';

  /**
   * getLayoutType
   * @return {String}
   */
  var getLayoutType = function () {
    return (window.matchMedia(MQforPC).matches) ? 'pc' : 'sp';
  };

  /**
   * isSpLayout
   * @return {Boolean}
   */
  var isSpLayout = function () {
    return getLayoutType() === 'sp';
  };

  /*utility
  ----------------------------------------------------------------------*/
  /**
  * UA判別
  */
  var UAINFO = (function(){
    var ua = navigator.userAgent.toLowerCase();
    //browser
    var ie = !!ua.match(/(msie|trident)/i);
    var edge = !!ua.match(/edge/i);
    var chrome = edge ? false : !!ua.match(/(chrome|crios)/i);
    var safari = (edge || chrome) ? false : !!ua.match(/safari/i);
    var firefox = !!ua.match(/firefox/i);
    //mobile device and os
    var iPhone = ua.indexOf('iphone') >= 0;
    var iPod = ua.indexOf('ipod') >= 0;
    var iPad = ua.indexOf('ipad') >= 0 || (ua.indexOf('macintosh') >= 0 && navigator.standalone != null);
    var iOS = (iPhone || iPod || iPad);
    var Android = ua.indexOf('android') >= 0;
    var TB = (iPad || (Android && ua.indexOf('mobile') < 0));
    var SP = (!TB && (iOS || Android));
    return {
      IE: ie,
      Edge: edge,
      Chrome: chrome,
      Safari: safari,
      Firefox: firefox,
      
      iOS: iOS,
      iOS_SP: (iOS && SP),
      iOS_TB: (iOS && TB),
      Android: Android,
      Android_SP: (Android && SP),
      Android_TB: (Android && TB),
      TB: TB,
      SP: SP,
      iOS_Android: (iOS || Android)
    };
  })();
  if(globalKey){window[globalKey].UAINFO = UAINFO;}

  /**
  * クエリデータ取得
  */
  var QUERYDATA = (function(){
    var data={};
    if(location.search){
      var arr=location.search.substring(1,location.search.length).split('&');
      for(var i=0;i<arr.length;i++){data[arr[i].split('=')[0]] = arr[i].split('=')[1];}
    }
    return data;
  })();
  if(globalKey){window[globalKey].QUERYDATA = QUERYDATA;}


  /*module
  ----------------------------------------------------------------------*/
  /**
  * スムーズスクロール
  * 対象　[href^="#"]
  */
  (function(){
    var speed = 300;
    var easing = 'swing';
    var $scroller = $('html,body');
    var $win = $(window);

    function move_scroll($target){
      var headH = isSpLayout() ? 60 : 0;
      var pos = $target.offset().top;
      $scroller.animate({scrollTop:pos - headH}, speed, easing);
    }

    $(document).on('click', '[href^="#"]', function(){
      var $this = $(this);
      var href = $this.attr('href');
      var $target = $(href === '#'? 'html' : href);
      if($target.length){
        move_scroll($target);
        return false;
      }else{
        return true;
      }
    });

    // load event
    $win.on('load',function (e){
      var hash = location.hash;
      //アンカーリンクがあった場合
      if($(hash).length){
        e.preventDefault();
        setTimeout(function(){
          move_scroll($(hash));
        },500);
      }
    });
  })();


  /**
  * タブ切替コンストラクタ
  * ボタンのクリックでボタンと対象エリアにアクティブクラスを付与
  * ボタンとエリアの紐付けはDOM順番に依存
  */
  var TabChange = function(opt){
    var thisO = this;
    var def = opt.def-0||0;
    this.curSeq = -1;
    this.$btns = opt.$btns;
    this.$areas = opt.$areas;
    this.activeBtnClass = opt.activeBtnClass || '';
    this.activeAreaClass = opt.activeAreaClass || '';
    this.onChanged = (typeof opt.onChanged === 'function')?opt.onChanged:function(){};
    this.$btns.each(function(seq){
      var $this = $(this);
      $this.on('click',function(){
        this.blur();
        thisO.changeTab(seq);
      });
    });
    this.changeTab(def,true);
  };
  TabChange.prototype.changeTab = function(seq,firstFlg){
    if(this.curSeq === seq){
      return;
    }
    var $activeArea = this.$areas.eq(seq);
    if(!firstFlg && $activeArea.is(':visible')){
      return;
    }
    this.$btns.removeClass(this.activeBtnClass);
    this.$btns.eq(seq).addClass(this.activeBtnClass);
    this.$areas.removeClass(this.activeAreaClass);
    $activeArea.addClass(this.activeAreaClass);
    this.curSeq = seq;
    this.onChanged(seq);
  };
  if(globalKey){window[globalKey].TabChange = TabChange;}



  $(function(){
    /**
    * ラッパー汎用型 TabChange の生成
    *  ラッパー　　　　：[data-cmnjs-tabchange-role="wrap"]
    *  ボタン　　　　　：[data-cmnjs-tabchange-role="wrap"] [data-cmnjs-tabchange-role="btn"]
    *  エリア　　　　　：[data-cmnjs-tabchange-role="wrap"] [data-cmnjs-tabchange-role="area"]
    *  アクティブボタン・エリアに付与されるクラス：tabActive
    *  ラッパーの data-cmnjs-tabchange-def 属性で初期タブのシーケンスを指定可
    * 
    * 追加機能
    * タブ外からタブの操作を行う。
    * ラッパー　　　　：[data-cmnjs-tabchange-name="name"]
    * タブ外のボタン　：[data-cmnjs-tabchange-operation-outside="name,num"]
    * 　　　　　　　　　　※nameは、ラッパーのnameと同一とする
    * 　　　　　　　　　　※numは、0始まりとする
    */
    var wrapData = 'data'+DATAPREF+'-tabchange-role=wrap';
    var btnData = 'data'+DATAPREF+'-tabchange-role=btn';
    var areaData = 'data'+DATAPREF+'-tabchange-role=area';
    var defaultSeqData = 'data'+DATAPREF+'-tabchange-def';
    var tabchangeBtnData = 'data'+DATAPREF+'-tabchange-btn';
    var activeClass = 'tabActive';

    var tabchangeNameData = 'data'+DATAPREF+'-tabchange-name';
    var tabchangeOperationOutsideData = 'data'+DATAPREF+'-tabchange-operation-outside';

    $('['+wrapData+']').each(function(i){
      var $wrap = $(this);
      var $nested = $wrap.find('['+wrapData+']'+' *');//ネストを考慮
      var $btns = $wrap.find('['+btnData+']').not($nested);
      var $areas = $wrap.find('['+areaData+']').not($nested);
      if($btns.length && $areas.length){
        var tabChangeObj = new TabChange({
          $btns: $btns,
          $areas: $areas,
          activeBtnClass: activeClass,
          activeAreaClass: activeClass,
          def: $wrap.attr(defaultSeqData),
        });

        // タブ外のボタンからタブの操作
        if($wrap.attr(tabchangeNameData)){
          var tabname = $wrap.attr(tabchangeNameData);
          $('['+tabchangeOperationOutsideData+'^="'+tabname+',"]').each(function(){
            var $operationOutsideBtn = $(this);
            var operationOutsideNum = parseInt($operationOutsideBtn.attr(tabchangeOperationOutsideData).split(',')[1],10);
            $operationOutsideBtn.on('click',function(){
              tabChangeObj.changeTab(operationOutsideNum);
            });
          });
        }

        // タブ内からタブの操作
        $wrap.find('['+tabchangeBtnData+']').each(function(){
          var $tabchangeBtn = $(this);
          $tabchangeBtn.on('click',function(){
            tabChangeObj.changeTab($tabchangeBtn.attr(tabchangeBtnData));
          });
        });
      }
    });
  });



  /**
  * アコーディオンコンストラクタ
  * ボタンのクリックでボタンにアクティブクラスを付与し、対応するエリアを開閉アニメーション
  * 開閉後displayスタイルを取り除き、エリアにアクティブクラスを付与する
  */
  var Accordion = function(opt){
    var thisO = this;
    this.$btn = opt.$btn;
    this.$area = opt.$area;
    this.activeBtnClass = opt.activeBtnClass || '';
    this.activeAreaClass = opt.activeAreaClass || '';
    this.openedFlg = opt.openedFlg;
    this.speed = opt.speed||200;
    this.onBeforeDisplayChange = opt.onBeforeDisplayChange || function(){};
    this.onAfterDisplayChange  = opt.onAfterDisplayChange  || function(){};
    this.busyFlg = false;
    this.useDisplayCheck = !!opt.useDisplayCheck;
    this.$btn.on('click',function(){
      this.blur();
      thisO.displayChange(!thisO.openedFlg);
    });
    this.displayChange(thisO.openedFlg, true, true);
  };
  Accordion.prototype.displayChange = function(flg, noAnimationFlg, initFlg){
    var visibleCheck,hasActiveClass;
    if(!initFlg && this.useDisplayCheck){
      if(flg){
        hasActiveClass = this.$area.hasClass(this.activeAreaClass);
        visibleCheck = this.$area.is(':visible');
        if(visibleCheck){return;}
        this.$area.addClass(this.activeAreaClass);
        visibleCheck = this.$area.is(':visible');
        if(!hasActiveClass){
          this.$area.removeClass(this.activeAreaClass);
        }
        if(!visibleCheck){return;}
      }else{
        hasActiveClass = this.$area.hasClass(this.activeAreaClass);
        visibleCheck = this.$area.is(':visible');
        if(!visibleCheck){return;}
        this.$area.removeClass(this.activeAreaClass);
        visibleCheck = this.$area.is(':visible');
        if(hasActiveClass){
          this.$area.addClass(this.activeAreaClass);
        }
        if(visibleCheck){return;}
      }
    }
    if(this.busyFlg){return;}
    this.busyFlg = true;
    if(this.onBeforeDisplayChange(flg) === false){
      this.busyFlg = false;
      return;
    }
    var thisO = this;
    var speed = noAnimationFlg? 0: this.speed;
    if(flg){//open
      this.$btn.addClass(this.activeBtnClass);
      this.$area.slideDown(speed,'swing',function(){thisO.displayChangeCallback(flg);});
    }else{//close
      this.$btn.removeClass(this.activeBtnClass);
      this.$area.slideUp(speed,'swing',function(){thisO.displayChangeCallback(flg);});
    }
  };
  Accordion.prototype.displayChangeCallback = function(flg){
    this.openedFlg = flg;
    if(flg){
      this.$area.addClass(this.activeAreaClass);
    }else{
      this.$area.removeClass(this.activeAreaClass);
    }
    this.$area.css({display:''});
    this.onAfterDisplayChange(flg);
    this.busyFlg = false;
  };
  if(globalKey){window[globalKey].Accordion = Accordion;}


  $(function(){
    /**
    * ラッパー汎用型Accordionの生成
    *  ラッパー：[data-cmnjs-accordion-role="wrap"]
    *  ボタン　：[data-cmnjs-accordion-role="wrap"] [data-cmnjs-accordion-role="btn"]
    *  エリア　：[data-cmnjs-accordion-role="wrap"] [data-cmnjs-accordion-role="area"]
    *  アクティブボタン・エリアに付与されるクラス：accordionActive
    *  ラッパーの data-cmnjs-accordion-active 属性の指定があれば初期状態で開く
    */
    var wrapData = 'data'+DATAPREF+'-accordion-role=wrap';
    var btnData = 'data'+DATAPREF+'-accordion-role=btn';
    var areaData = 'data'+DATAPREF+'-accordion-role=area';
    var activeClass = 'accordionActive';
    var defaultOpendData = 'data'+DATAPREF+'-accordion-active';

    //ラッパー汎用型生成
    $('['+wrapData+']').each(function(){
      var $wrap = $(this);
      var $nested = $wrap.find('['+wrapData+']'+' *');//ネストを考慮
      var $btn = $wrap.find('['+btnData+']').not($nested);
      var $area = $wrap.find('['+areaData+']').not($nested);
      if($btn.length && $area.length){
        new Accordion({
          $btn: $btn,
          $area: $area,
          activeBtnClass: activeClass,
          activeAreaClass: activeClass,
          openedFlg: (typeof $wrap.attr(defaultOpendData) !== 'undefined'),
          useDisplayCheck: true
        });
      }
    });
  });

  /*
  * windowスクロール監視コンストラクタ
  */
  var Scrollwatch = function(opt){
    var thisO = this;
    this.$target = opt.$target;
    this.scrolledClass = opt.scrolledClass || '';
    this.scrollThreshold = opt.scrollThreshold || 1;
    $(window).on('scroll resize',function(){
      thisO.checkScroll();
    });
    this.checkScroll();
  };
  Scrollwatch.prototype.checkScroll = function(){
    if(window.pageYOffset >= this.scrollThreshold){
      this.$target.addClass(this.scrolledClass);
    }else{
      this.$target.removeClass(this.scrolledClass);
    }
  };
  if(globalKey){window[globalKey].Scrollwatch = Scrollwatch;}



  $(function(){
    /*
    * 汎用windowスクロール監視
    * 対象要素：[data-cmnjs-scrollwatch]
    * 100px以上スクロールした場合に付与するクラス：scrollwatchScrolled
    */
    var scrollwatchData = 'data'+DATAPREF+'-scrollwatch';
    $('['+scrollwatchData+']').each(function(){
      var $this = $(this);
      var param = $this.attr(scrollwatchData);
      var defaultNum = 100;
      param = param?Number(param):defaultNum;
      new Scrollwatch({
        $target: $(this),
        scrolledClass: 'scrollwatchScrolled',
        scrollThreshold: isNaN(param)?defaultNum:param
      });
    });
  });


  /*
  * ポジション監視コンストラクタ
  */
  var Poswatch = function(opt){
    var thisO = this;
    this.$target = opt.$target;
    this.infrontClass = opt.infrontClass || '';
    this.posClasses = $.extend({
      bottomDw: '',
      bottomOn: '',
      bottomUp: '',
      topDw: '',
      topOn: '',
      topUp: ''
    },opt.posClasses || {});

    $(window).on('scroll resize',function(){
      thisO.checkPosition();
    });
    this.checkPosition();
  };
  Poswatch.prototype.checkPosition = (function(){
    var $body = $();
    $(function(){$body = $('body');});
    return function(){
      //ベース位置の通過判定
      var bodyBorderTop =  0;//offset().topがbodyのborderTopを反映しないため補正
      if($body.length){
        bodyBorderTop = Number($body.css('borderTopWidth').replace('px',''));
        bodyBorderTop = isNaN(bodyBorderTop)?0:bodyBorderTop;
      }
      var targetH = this.$target.outerHeight();
      var targetT = this.$target.offset().top+bodyBorderTop;

      if(window.pageYOffset > targetT+targetH){
        this.$target.removeClass(this.posClasses.topOn+' '+this.posClasses.topDw).addClass(this.posClasses.topUp);
      }else if(window.pageYOffset > targetT){
        this.$target.removeClass(this.posClasses.topUp+' '+this.posClasses.topDw).addClass(this.posClasses.topOn);
      }else{
        this.$target.removeClass(this.posClasses.topUp+' '+this.posClasses.topOn).addClass(this.posClasses.topDw);
      }
      if(window.pageYOffset+window.innerHeight < targetT){
        this.$target.removeClass(this.posClasses.bottomUp+' '+this.posClasses.bottomOn).addClass(this.posClasses.bottomDw);
      }else if(window.pageYOffset+window.innerHeight < targetT+targetH){
        this.$target.removeClass(this.posClasses.bottomUp+' '+this.posClasses.bottomDw).addClass(this.posClasses.bottomOn);
      }else{
        this.$target.removeClass(this.posClasses.bottomDw+' '+this.posClasses.bottomOn).addClass(this.posClasses.bottomUp);
      }
    };
  })();
  if(globalKey){window[globalKey].Poswatch = Poswatch;}



  $(function(){
    /*
    * 汎用ポジション監視
    * 対象要素：[data-cmnjs-poswatch]
    * 要素がウィンドウの下辺よりも下にいる場合に付与するクラス:poswatchBottomDw
    * 要素がウィンドウの下辺に重なっている場合に付与するクラス:poswatchBottomOn
    * 要素がウィンドウの下辺よりも上にいる場合に付与するクラス:poswatchBottomUp
    * 要素がウィンドウの上辺よりも下にいる場合に付与するクラス:poswatchTopDw
    * 要素がウィンドウの上辺に重なっている場合に付与するクラス:poswatchTopOn
    * 要素がウィンドウの上辺よりも上にいる場合に付与するクラス:poswatchTopUp
    */
    var poswatchData = 'data'+DATAPREF+'-poswatch';
    $('['+poswatchData+']').each(function(){
      new Poswatch({
        $target: $(this),
        posClasses: {
          bottomDw: 'poswatchBottomDw',
          bottomOn: 'poswatchBottomOn',
          bottomUp: 'poswatchBottomUp',
          topDw: 'poswatchTopDw',
          topOn: 'poswatchTopOn',
          topUp: 'poswatchTopUp',
        }
      });
    });
  });

  /**
  * ヘッダーグロナビ
  */
  $(function(){
    var $wrap_gnav = $('.wrap-gnav');
    // SP
    var $nav_sp = $wrap_gnav.find('.nav-sp');
    var $nav_sp_parent_btn = $nav_sp.find('.btn-tgl-nav'); //ハンバーガーメニュー
    var $nav_sp_parent_wrap = $nav_sp.find('.wrap-nav-sp-parent');
    var $nav_sp_child_btn = $nav_sp.find('.btn-open-child');

    var sp_menu_flg = false;

    // PC
    var $pc_nav_btn = $('.l-head .lst-category-nav .category-nav-link.has-sub');
    var $pc_nav_li = $pc_nav_btn.closest('li.item');
    var pc_menu_flg = false;

    // 検索窓
    var $nav_search = $wrap_gnav.find('.nav-support .nav-search');
    var $nav_search_btn = $nav_search.find('.btn-search');

    var ACTIVE_CLASS = 'active';
    var CHILD_OPEN_CLASS = 'child-open';
    var SLIDE_SPEED = 300;
    var FADE_SPEED = 300;

    /**
    * メニュー内のアコーディオン
    *  ラッパー：[data-accordion-spheadnav-role="wrap"]
    *  ボタン　：[data-accordion-spheadnav-role="wrap"] [data-accordion-spheadnav-role="btn"]
    *  エリア　：[data-accordion-spheadnav-role="wrap"] [data-accordion-spheadnav-role="area"]
    *  アクティブボタン・エリアに付与されるクラス：accordionActive
    *  ラッパーの data-accordion-spheadnav-active 属性の指定があれば初期状態で開く
    */
    var spAccWrapData = 'data-accordion-spheadnav-role=wrap';
    var spAccBtnData = 'data-accordion-spheadnav-role=btn';
    var spAccAreaData = 'data-accordion-spheadnav-role=area';
    var spAccActiveClass = 'accordionActive';
    var spAccDefaultOpendData = 'data-accordion-spheadnav-active';
    var spAccObjAry = [];

    // SPメニューリセット
    function sp_reset_menu(){
      // 孫メニューリセット
      if($nav_sp_parent_wrap.find('.wrap-nav-sp-child.active').length){
        var $child_active_elm = $nav_sp_parent_wrap.find('.wrap-nav-sp-child.active');
        $child_active_elm.slideUp(SLIDE_SPEED,function(){
          $child_active_elm.css('display','');
          $child_active_elm.removeClass(ACTIVE_CLASS);
        });
      }
      // ハンバーガーメニュー閉じる
      $nav_sp_parent_wrap.slideUp(SLIDE_SPEED,function(){
        $nav_sp_parent_btn.removeClass(ACTIVE_CLASS);
        $nav_sp_parent_wrap.removeClass(ACTIVE_CLASS);
        $nav_sp_parent_wrap.removeClass('child-open');
        $nav_sp_parent_wrap.find('.wrap-nav-sp-child').removeClass(ACTIVE_CLASS);

        //SPアコーディオンメニュー リセット
        spAccObjAry.forEach(function(obj){
          obj.displayChange(false,true);
        });
        sp_menu_flg = false;
      });
    }




    // SPヘッダー内のアコーディオン開閉
    $nav_sp_parent_wrap.find('['+spAccWrapData+']').each(function(){
      var $spAccWrap = $(this);
      var $spAccNested = $spAccWrap.find('['+spAccWrapData+']'+' *');//ネストを考慮
      var $spAccBtn = $spAccWrap.find('['+spAccBtnData+']').not($spAccNested);
      var $spAccArea = $spAccWrap.find('['+spAccAreaData+']').not($spAccNested);

      if($spAccBtn.length && $spAccArea.length){
        spAccObjAry.push(new Accordion({
          $btn: $spAccBtn,
          $area: $spAccArea,
          activeBtnClass: spAccActiveClass,
          activeAreaClass: spAccActiveClass,
          openedFlg: (typeof $spAccWrap.attr(spAccDefaultOpendData) !== 'undefined'),
          useDisplayCheck: false
        }));
      }
    });

    // ハンバーガーメニュー開閉
    $nav_sp_parent_btn.on('click',function(){
      if(!sp_menu_flg){
        sp_menu_flg = true;
        var $this = $(this);
        if($this.hasClass(ACTIVE_CLASS)){
          sp_reset_menu();
        }else{
          $nav_search.removeClass(ACTIVE_CLASS);
          $nav_sp_parent_wrap.addClass(ACTIVE_CLASS);
          $nav_sp_parent_wrap.slideDown(SLIDE_SPEED,function(){
            $this.addClass(ACTIVE_CLASS);
            sp_menu_flg = false;
          });
        }
      }
    });

    // 孫メニュー開閉
    $nav_sp_child_btn.each(function(){
      var $childOpenBtn = $(this);
      var $childWrap = $childOpenBtn.next('.wrap-nav-sp-child');
      var $childCloseBtn = $childWrap.find('.btn-close-child');

      $childOpenBtn.on('click',function(){
        $nav_sp_parent_wrap.addClass(CHILD_OPEN_CLASS);
        $childWrap.addClass(ACTIVE_CLASS);
      });
      $childCloseBtn.on('click',function(){
        $nav_sp_parent_wrap.removeClass(CHILD_OPEN_CLASS);
        $childWrap.removeClass(ACTIVE_CLASS);
      });
    });

    //検索ボタン
    $nav_search_btn.on('click',function(){
      if(!sp_menu_flg){
        sp_reset_menu();
        if($nav_search.hasClass(ACTIVE_CLASS)){
          $nav_search.removeClass(ACTIVE_CLASS);
        }else{
          $nav_search.addClass(ACTIVE_CLASS);
        }
      }
    });

    // PCナビ
    $pc_nav_li.hover(
      function(){
        var $this = $(this);
        $this.find('.category-nav-link.has-sub').addClass(ACTIVE_CLASS);
        $this.find('.wrap-lst-sub').stop().fadeIn(FADE_SPEED);
      },
      function(){
        var $this = $(this);
        $this.find('.category-nav-link.has-sub').removeClass(ACTIVE_CLASS);
        $this.find('.wrap-lst-sub').stop().fadeOut(FADE_SPEED);
      }
    );

    // レイアウト切り替えで開いていたメニューを閉じる
    window.matchMedia(MQforPC).addListener(function (e) {
      if(e.matches){
        //PC時、SPメニューのリセット
        if($nav_sp_parent_btn.hasClass(ACTIVE_CLASS)){
          sp_reset_menu();
        }
      }else{
        // SP時、PCメニューのリセット
        if($pc_nav_btn.hasClass(ACTIVE_CLASS)){
          $pc_nav_btn.removeClass(ACTIVE_CLASS);
          $pc_nav_btn.closest('li').find('.wrap-lst-sub').css('display','');
        }
      }
      // 検索メニュー閉じる
      if($nav_search.hasClass(ACTIVE_CLASS)){
        $nav_search.removeClass(ACTIVE_CLASS);
      }
    });
  });


  /**
  * ローカルナビ
  */
  $(function(){
    var $body = $('body');
    var current_id = $body.data('lnav-current-id');
    var $navi = $('.lst-lnav');
    var OPEN = 'open';
    var ACTIVE = 'active';
    var $current_li = $navi.find('[data-lnav-current-id="'+current_id+'"]');
    var slide_flg = false;
    var SLIDE_SPEED = 300;

    $current_li.addClass(OPEN);
    $current_li.parents('li[data-lnav-current-id]').addClass(OPEN);

    $navi.find('[data-lnav-current-id="'+current_id+'"] > .lnav-link').addClass(ACTIVE);
    $navi.find('li[data-lnav-current-id].open > .lnav-lbl').addClass(ACTIVE);
    $navi.find('li[data-lnav-current-id].open > .lnav-lbl + .lst-lnav').addClass(ACTIVE);

    $navi.find('.lnav-lbl').on('click',function(){
      if(!slide_flg){
        var $this = $(this);
        var $next = $this.next();
        slide_flg = true;

        if($next.hasClass(ACTIVE)){
          $next.slideUp(SLIDE_SPEED,function(){
            slide_flg = false;
          });
          $this.removeClass(ACTIVE);
          $next.removeClass(ACTIVE);
        }else{
          $next.slideDown(SLIDE_SPEED,function(){
            slide_flg = false;
          });
          $this.addClass(ACTIVE);
          $next.addClass(ACTIVE);
        }
      }
    });
  });


  /*
  * 同意チェック
  * チェックボックス：[data-js-agree-check="checkbox"]
  * ボタン：[data-js-agree-check="btn"]
  * $agree_checkの数とcheckの数が同一の場合、ボタンを活性化させる
  */
  $(function(){
    var $agree_check = $('input[type="checkbox"][data-js-agree-check="checkbox"]');
    var $agree_btn = $('button[data-js-agree-check="btn"]');
    var DISABLED_CLASS = 'btn-disabled';
    var check_len = $agree_check.length;

    function agree_check(){
      if($('input[type="checkbox"][data-js-agree-check="checkbox"]:checked').length === check_len){
        $agree_btn.prop('disabled',false);
        $agree_btn.removeClass(DISABLED_CLASS);
      }else{
        $agree_btn.prop('disabled',true);
        $agree_btn.addClass(DISABLED_CLASS);
      }
    }

    $agree_check.on('change',function(){
      agree_check();
    });
    agree_check();
  });

})(jQuery);