(function($){
  'use strict';
  /*
  * 同意チェック
  * チェックボックス：[data-js-agree-check="checkbox"]
  * ボタン：[data-js-agree-check="btn"]
  * $agree_checkの数とcheckの数が同一の場合、ボタンを活性化させる
  */
  $(function(){
    var $agree_check = $('input[type="checkbox"][data-js-agree-check="checkbox"]');
    var $agree_btn = $('button[data-js-agree-check="btn"]');
    var DISABLED_CLASS = 'btn-disabled';
    var check_len = $agree_check.length;

    function agree_check(){
      if($('input[type="checkbox"][data-js-agree-check="checkbox"]:checked').length === check_len){
        $agree_btn.prop('disabled',false);
        $agree_btn.removeClass(DISABLED_CLASS);
      }else{
        $agree_btn.prop('disabled',true);
        $agree_btn.addClass(DISABLED_CLASS);
      }
    }

    $agree_check.on('change',function(){
      agree_check();
    });
    agree_check();
  });

})(jQuery);